import requests
import pandas as pd
import datetime
from csv import DictWriter
import time
import numpy as np
import ssl
import urllib.request, urllib.parse, json
results = []


class Memoize:
    def __init__(self, f):
        self.f = f
        self.memo = {}

    def __call__(self, *args):
        if not args[1] in self.memo:
            self.memo[args[1]] = {len(args[0]): self.f(*args)}
        if not len(args[0]) in self.memo[args[1]]:
            self.memo[args[1]][len(args[0])] = self.f(*args)
        return self.memo[args[1]][len(args[0])]


@Memoize
def ema(prices, n):
    if len(prices) == 1:
        return prices[0]
    last, prices = prices[-1], prices[:-1]
    return last/n + (1-1/n) * ema(prices, n)


def gen_signal(vector):
    return (vector * np.exp(-1 * np.square(vector)/4))/(np.sqrt(2) * np.exp(-0.5))


def make_url(symbol, startTime=False):
    if not startTime:
        url = f"https://www.bitmex.com/api/v1/trade/bucketed?binSize=1h&partial=false&symbol=XBTUSD&columns=close&count=500&reverse=false"
        return url
    url = f"https://www.bitmex.com/api/v1/trade/bucketed?binSize=1h&partial=false&symbol=XBTUSD&columns=close&count=500&reverse=false&startTime={startTime}"
    return url


def main(symbol, startTime=False):
    r = requests.get(make_url(symbol, startTime))
    if int(r.headers['x-ratelimit-remaining']) == 0:
        time.sleep(int(r.headers['x-ratelimit-reset']
                       ) - round(time.time()) + 1)
    r = r.json()
    start = datetime.datetime.strptime(
        r[-1]['timestamp'], "%Y-%m-%dT%H:%M:%S.%fZ") + datetime.timedelta(hours=1)
    start = start.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    for quote in r:
        quote.pop('symbol')
        results.append(quote)
    return start


def get_signal(quotes):
    quotes.reset_index(drop=True, inplace=True)
    quotes['timestamp'] = pd.to_datetime(
        quotes['timestamp'], format='%Y-%m-%dT%H:%M:%S.%fZ')

    quotes['ema_8'] = quotes.close.expanding().apply(ema, raw=True, args=[8])
    quotes['ema_24'] = quotes.close.expanding().apply(ema, raw=True, args=[24])
    quotes['ema_168'] = quotes.close.expanding().apply(
        ema, raw=True, args=[168])
    quotes['ema_72'] = quotes.close.expanding().apply(ema, raw=True, args=[72])
    quotes['ema_504'] = quotes.close.expanding().apply(
        ema, raw=True, args=[504])

    quotes['diff1'] = quotes.ema_8 - quotes.ema_24
    quotes['diff2'] = quotes.ema_24 - quotes.ema_72
    quotes['diff3'] = quotes.ema_168 - quotes.ema_504

    quotes['y1'] = quotes['diff1']/quotes.close.rolling(168).std()
    quotes['y2'] = quotes['diff2']/quotes.close.rolling(168).std()
    quotes['y3'] = quotes['diff3']/quotes.close.rolling(168).std()

    quotes['z1'] = quotes['y1']/quotes.y1.rolling(720).std()
    quotes['z2'] = quotes['y2']/quotes.y2.rolling(720).std()
    quotes['z3'] = quotes['y3']/quotes.y3.rolling(720).std()

    quotes['u1'] = gen_signal(quotes['z1'])
    quotes['u2'] = gen_signal(quotes['z2'])
    quotes['u3'] = gen_signal(quotes['z3'])

    quotes['signal'] = (quotes.u1 + quotes.u2 + quotes.u3)/3

    return quotes.tail(1).signal.values[0]

def SendMessageToSlack(text):
    context = ssl._create_unverified_context()
    post = {"text": "```{0}```".format(text)}

    try:
        json_data = json.dumps(post)
        req = urllib.request.Request(
        "https://hooks.slack.com/services/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",
        data=json_data.encode('ascii'),
        headers={'Content-Type': 'application/json'}
        )
    
        resp = urllib.request.urlopen(req,context = context)
    except Exception as em:
        print("EXCEPTION: " + str(em))

res = requests.get("https://www.bitmex.com/api/v1/trade/bucketed?binSize=1h&partial=false&symbol=XBTUSD&columns=close&count=1&reverse=true").json()
close_time = res[0]["timestamp"]
close_price = res[0]["close"] 

start_time = main('XBTUSD')
quotes = pd.DataFrame(results)
results = []
while datetime.datetime.strptime(start_time, "%Y-%m-%dT%H:%M:%S.%fZ") < datetime.datetime.utcnow():
    start_time = main('XBTUSD', start_time)
    temp = pd.DataFrame(results)
    quotes = pd.concat([quotes, temp])
    results = []

SendMessageToSlack(f"Timestamps: {close_time}\nLast close price: {close_price}\nPrice m-signal: {get_signal(quotes)}\nShort-term(u1): {quotes.tail(1).u1.values[0]}\nMedium-term(u2): {quotes.tail(1).u2.values[0]}\nLong-term(u3): {quotes.tail(1).u3.values[0]}")
