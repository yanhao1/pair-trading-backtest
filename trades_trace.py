import sys
import json
import csv
import time
import datetime
import asyncio
import requests
import aiohttp


def weekly_trade_data(outputFile, start, end):

    URLString   = 'https://stellar-horizon.satoshipay.io'

    #create the csv file
    file = open(outputFile, 'w', newline='')

    writer = csv.writer(file)
    writer.writerow(['Date', 'Sell Asset', 'Sell Asset Issuer', 'Sell Amount', 'Buy Asset', 'Buy Asset Issuer', 'BuyAmount', 'Seller', 'Buyer', 'Operation ID'])
    file.close()

    #query for sequence number
    #response = requests.get(URLString + '/ledgers?order=desc').json()

    tasks = []
    responses = []
    group_size = 432

    sequences = []
    sequence_package = [] # a list of sequence groups

    for i in range(start, end):
        sequences.append(i)

    sequence_package = [sequences[j:j+group_size] for j in range(0, len(sequences), group_size)]

    async def write_data_to_csv(responses):
        for response in responses:
            response = json.loads(response)
            if 'status' in response and response['status'] == 404:
                print(response)
                print("ERROR! Check address, Server and/or Network connection. Exiting.")
                sys.exit(1)

            response = response["_embedded"]["records"]
            ID = []
            for j in range(len(response)):
                if response[j]["type"] == "trade":
                    operation_ID, num = response[j]["id"].split("-")
                    
                    if operation_ID not in ID:
                        ID.append(operation_ID)
                        #result = requests.get(URLString + "/operations/" + str(response[j]["id"])).json()
                        result = response[j]
                        Time = result["created_at"]
                        #Account = result["source_account"]
                        
                        
                        Seller = result["seller"]
                        Buyer = result["account"]

                        #check whether it is an arbitrage pathpayment
                        Sold_type = result["sold_asset_type"]
                        if Sold_type == "native":
                            sold_asset = "XLM"
                            soold_asset_issuer = "native"
                        else:
                            sold_asset = result["sold_asset_code"]
                            sold_asset_issuer = result["sold_asset_issuer"]
                        Bought_type = result["bought_asset_type"]
                        if Bought_type == "native":
                            bought_asset = "XLM"
                            bought_asset_issuer = "native"
                        else:
                            bought_asset = result["bought_asset_code"]
                            bought_asset_issuer = result["bought_asset_issuer"]
                        sold_amount = result["sold_amount"]
                        bought_amount = result["bought_amount"]

                        with open(outputFile, 'a', newline='') as f:
                            writer = csv.writer(f)
                            writer.writerow([Time, sold_asset, sold_asset_issuer, sold_amount, bought_asset, bought_asset_issuer, bought_amount, Seller, Buyer, [operation_ID]])
                            f.close()

    async def fetch(session, url):
        async with session.get(url) as response:
            return await response.text()


    async def main():
        async with aiohttp.ClientSession() as session:
            for sequence_group in sequence_package:
                for sequences in sequence_group:
                    url = URLString + "/ledgers/" + str(sequences) + "/effects?order=desc"
                    task = asyncio.ensure_future(fetch(session, url))
                    tasks.append(task)
                responses = await asyncio.gather(*tasks)
        await write_data_to_csv(responses)

    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(main())
    loop.run_until_complete(future)
