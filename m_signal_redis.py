import serialized_redis
import ssl
import urllib.request, urllib.parse, json

def get_signal():
    Host = "database.muddyblocks.com"
    Password = "K_Ste11ar_B"
    redisInstance = serialized_redis.PickleSerializedRedis(host=Host, password=Password)

    m_signal_key = ('kb-cyberport-env::exchanges::signals::momentum::XBTUSD')
    m_signals = redisInstance.get(m_signal_key)["data"]

    m_signal = m_signals["m"]
    date = m_signals["last-update"]
    time = date[:4] + "-" + date[4:6] + "-" + date[6:8] + date[8:] + ":00:00UTC" 

    return m_signal, time

def SendMessageToSlack(text):
    context = ssl._create_unverified_context()
    post = {"text": "```{0}```".format(text)}

    try:
        json_data = json.dumps(post)
        req = urllib.request.Request(
        "https://hooks.slack.com/services/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",
        data=json_data.encode('ascii'),
        headers={'Content-Type': 'application/json'}
        )

        resp = urllib.request.urlopen(req,context = context)
    except Exception as em:
        print("EXCEPTION: " + str(em))

m_signal, time = get_signal()
SendMessageToSlack(f"m-signal from redis\nTimestamps: {time}\nm-signal: {m_signal}")

