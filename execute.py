from trades_trace import weekly_trade_data
import time

init = 15383682
total_weeks = 2

ledgers_weekly = 86400 / 5 * 7 # 86400s per day, 5s per ledger

for i in range(total_weeks):
    start = init + i*ledgers_weekly
    end = init + (i+1)*ledgers_weekly
    outputFile = f'trades_for_week{i}.csv'
    weekly_trade_data(outputFile, int(start), int(end))
    time.sleep(600)  # sleep 10 min before next move
    