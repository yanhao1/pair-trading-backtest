import pandas as pd
from datetime import datetime
pd.options.mode.chained_assignment = None
import multiprocessing
import statsmodels.api as sm
import numpy as np

hedge1 = 0
hedge2 = 0
constant1 = 0
constant2 = 0

def cleanData(file):
    global hedge1, hedge2, constant1, constant2
    # Read data and split into two dataframes
    quotes = pd.read_csv(file)
    quotes = quotes[(quotes.symbol == 'XBTUSD') | (quotes.symbol == 'XBTM19')]
    quotes['timestamp'] = pd.to_datetime(quotes['timestamp'], format='%Y-%m-%dD%H:%M:%S.%f')
    quotes.sort_values('timestamp', inplace=True)
    perpetual_quotes = quotes[quotes['symbol'] == 'XBTUSD']
    future_quotes = quotes[quotes['symbol'] == 'XBTM19']

    # Rename columns
    perpetual_quotes.columns = ['timestamp', 'symbol', 'perpBidSize', 'perpBidPrice', 'perpAskPrice', 'perpAskSize']
    future_quotes.columns = ['timestamp', 'symbol', 'futureBidSize', 'futureBidPrice', 'futureAskPrice', 'futureAskSize']
    # perpetual_quotes.reset_index(drop=True, inplace=True)
    # future_quotes.reset_index(drop=True, inplace=True)

    # refactor: drop duplicates from future and perpetual after declaring same time to avoid last 3 lines
    same_time_quotes = quotes[quotes.timestamp.isin(quotes.timestamp[quotes.timestamp.duplicated()])]
    perpetual_quotes.drop_duplicates('timestamp', 'last', True)
    future_quotes.drop_duplicates('timestamp', 'last', True)
    no_dup_quotes = quotes[~quotes.timestamp.isin(quotes.timestamp[quotes.timestamp.duplicated()])]
    no_dup_quotes = no_dup_quotes.append(same_time_quotes[same_time_quotes['symbol'] == 'XBTUSD']) #hope for the best, is not a long term solution



    future_quotes = future_quotes.set_index('timestamp').reindex(no_dup_quotes['timestamp']).reset_index()
    perpetual_quotes = perpetual_quotes.set_index('timestamp').reindex(no_dup_quotes['timestamp']).reset_index()
    
    future_quotes.sort_values('timestamp', inplace=True)
    perpetual_quotes.sort_values('timestamp', inplace=True)

    perpetual_quotes.fillna(method='ffill', inplace=True)
    future_quotes.fillna(method='ffill', inplace=True)


    future_quotes.drop(columns='symbol', inplace=True)
    perpetual_quotes.drop(columns='symbol', inplace=True)

    prices = perpetual_quotes.merge(future_quotes, on='timestamp')
    prices.dropna(inplace=True)
    prices.sort_values('timestamp', inplace=True)
    prices.set_index('timestamp', inplace=True)


    prices['hedge1'] = hedge1
    prices['hedge2'] = hedge2
    
    #fix this code so this isnt 2 steps
    # types = {'perpBidSize': 'uint32', 'perpBidPrice': 'float16', 'perpAskPrice': 'float16', 'perpAskSize': 'uint32', 'futureBidSize': 'uint32', 'futureBidPrice': 'float16', 'futureAskPrice': 'float16', 'futureAskSize': 'uint32', 'hedge1': 'float32', 'hedge2': 'float32', 'constant1': 'float32', 'constant2': 'float32'}
    # prices.astype(types, copy=False)

    hedge1 = sm.OLS(prices['perpAskPrice'], prices['futureBidPrice']).fit().params[0]
    hedge2 = sm.OLS(prices['perpBidPrice'], prices['futureAskPrice']).fit().params[0]
    #hedge1 = 1.002382
    #hedge2 = 1.002382

    return hedge1, hedge2

beta1, beta2 = cleanData(test.csv)
print("beta1: ", beta1)
print("beta2: ", beta2)
print(price)
