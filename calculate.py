from data_cleaning import processData, cleanData
from backtest2 import Backtester2

quotes = processData(list_of_file_names)

quotes['diff1']  = quotes['perpAskPrice'] - quotes['hedge1'] * quotes['futureBidPrice'] 
quotes['diff2']  = quotes['perpBidPrice'] - quotes['hedge2'] * quotes['futureAskPrice']

mean1 = quotes.diff1.mean()
mean2 = quotes.diff2.mean()
std1 = quotes.diff1.std()
std2 = quotes.diff2.std()

print("mean1: ", mean1)
print("mean2: ", mean2)
print("std1: ", std1)
print("std2: ", std2)



