from data_cleaning import cleanData
from csv import DictWriter
from datetime import datetime, timedelta
class Backtester2:
    def __init__(self):
        self.in_position = False
        self.trades = []
        self.positions = []
        self.profit = []
        self.max = 5
        self.bets = 0
        self.winners = 0
        self.trade_time = 0
        self.close_time = 0
        self.last = []
        self.sustained_time = 0
        self.next_trade_time = 0
        self.net_position = {'perp': 0, 'perp_price':0, 'future': 0, 'future_price': 0}
        self.total_capital = 5
        self.market_volume = 0
        self.fees = 0
        self.delever_prices = (0,0)
        self.zero_prices = (0,0)
        self.liqudated = False
        self.liquidation_prices = (0,0)
    
    #only one position at a time
    #instant trading
    def can_trade(self, quote, z):
        try:
            self.last.perpAskPrice
        except AttributeError:
            return False
        if self.max <= 0.01:
            return False
        if self.in_position and (abs(self.net_position['perp']/self.net_position['perp_price']) + abs(self.net_position['future']/self.net_position['future_price']))/(1 * self.max) >= 1:
            return False
        if self.market_volume > 92:
            return False
        if self.trade_time == 0:
            if not self.in_position:
                if quote.z1 < -z and quote.z1 > -3:
                    self.tradeType = "undervalued"
                    self.trade_time = quote.Index + timedelta(seconds=0.5)
                    self.perp = quote.perpAskPrice
                    self.future = quote.futureBidPrice
                    self.hedge = quote.hedge1
                    self.get_responsible_asset(quote)
                    self.signal = {'time': quote.Index, 'perp': self.perp, 'perp_size': 0, 'future': self.future, 'future_size': 0, 'tradeType': 'enter signal', 'z': quote.z1}
                elif quote.z2 > z and quote.z2 < 3:
                    self.tradeType = "overvalued"
                    self.trade_time = quote.Index + timedelta(seconds=0.5)
                    self.perp = quote.perpBidPrice
                    self.future = quote.futureAskPrice
                    self.hedge = quote.hedge2
                    self.get_responsible_asset(quote)
                    self.signal = {'time': quote.Index, 'perp': self.perp, 'perp_size': 0, 'future': self.future, 'future_size': 0, 'tradeType': 'enter signal', 'z': quote.z2}
            else:
                if quote.z1 < -z and quote.z1 > -3 and self.tradeType == "undervalued":
                    self.trade_time = quote.Index + timedelta(seconds=0.5)
                    self.perp = quote.perpAskPrice
                    self.future = quote.futureBidPrice
                    self.hedge = quote.hedge1
                    self.get_responsible_asset(quote)
                    self.signal = {'time': quote.Index, 'perp': self.perp, 'perp_size': 0, 'future': self.future, 'future_size': 0, 'tradeType': 'enter signal', 'z': quote.z1}
                elif quote.z2 > z and quote.z2 < 3 and self.tradeType == "overvalued":
                    self.trade_time = quote.Index + timedelta(seconds=0.5)
                    self.perp = quote.perpBidPrice
                    self.future = quote.futureAskPrice
                    self.hedge = quote.hedge2
                    self.get_responsible_asset(quote)
                    self.signal = {'time': quote.Index, 'perp': self.perp, 'perp_size': 0, 'future': self.future, 'future_size': 0, 'tradeType': 'enter signal', 'z': quote.z2}
            return False
        return quote.Index > self.trade_time
    
    #investigate hedging based on current hedge ratio
    def trade(self, quote):
        # maximum = self.get_max_available_size(quote)
        # if self.delever_prices[0] != 0 and self.delever_prices[1] != 0:
        #     print(self.delever_prices)
        #     print(self.get_zero_contract_prices())
        #     print(self.get_zero_contract_sizes(quote))
        if self.tradeType == "undervalued":
            max_size = min(quote.perpAskSize, quote.futureBidSize, round(0.375 * self.max * quote.futureBidPrice))
            if max_size == quote.futureBidSize:
                perp_size = round(max_size * quote.hedge1)
                future_size = -1 * max_size
            else:
                perp_size = max_size
                future_size = -1 * round(max_size/quote.hedge1)
            perp_price = quote.perpAskPrice
            future_price = quote.futureBidPrice
            z = quote.z1
        else:
            max_size = min(quote.perpBidSize, quote.futureAskSize, round(0.375 * self.max * quote.futureAskPrice))
            if max_size == quote.futureAskSize:
                perp_size = -1 * round(max_size * quote.hedge2)
                future_size = max_size
            else:
                perp_size = -1 * max_size
                future_size = round(max_size/quote.hedge2)
            perp_price = quote.perpBidPrice
            future_price = quote.futureAskPrice
            z = quote.z2
        perp = perp_size/perp_price
        future = future_size/future_price
        self.max -= 0.01075 * (abs(perp) + abs(future))
        self.fees += 0.00075 * (abs(perp) + abs(future))
        self.market_volume += abs(perp) + abs(future)
        self.position  = {'time': quote.Index, 'perp': perp, 'perp_size': perp_size, 'future': future, 'future_size': future_size, 'tradeType': self.tradeType, 'z': z}
        self.net_position['perp_price'] =abs((self.net_position['perp'] * self.net_position['perp_price'] + perp_size * perp_price)/(self.net_position['perp'] + perp_size))
        self.net_position['future_price'] =abs((self.net_position['future'] * self.net_position['future_price'] + future_size * future_price)/(self.net_position['future'] + future_size))
        self.net_position['perp'] += perp_size
        self.net_position['future'] += future_size
        self.in_position = True
        self.trades.append(self.position)
        self.positions.append(self.position)
        self.trade_time = 0
        self.next_trade_time = quote.Index + timedelta(seconds = 2)
        self.liquidation_prices = self.get_liquidation_prices()

    def limit_trade(self, quote, z):
        if not self.in_position or quote.Index > self.next_trade_time:
            self.trades.append(self.signal)
            if not self.perpResponsible:
                if self.tradeType == "undervalued":
                    minPrice = (self.perp + z * quote.std1 - quote.mean1)/self.hedge
                    minPrice = round(minPrice * 2)/2
                    if quote.futureBidPrice < minPrice:
                        self.trade_time = 0
                    else:
                        self.trade(quote)
                        self.closeType = 'overvalued'
                else:
                    minPrice = round((self.perp - quote.std2 * z - quote.mean2) * 2/self.hedge)/2
                    if quote.futureAskPrice > minPrice:
                        self.trade_time = 0
                    else:
                        self.trade(quote)
                        self.closeType = 'undervalued'
            else:
                if self.tradeType == "undervalued":
                    minPrice = round((-z * quote.std1 + quote.mean1 + self.hedge * self.future) * 2)/2
                    if quote.perpAskPrice > minPrice:
                        self.trade_time = 0
                    else:
                        self.trade(quote)
                        self.closeType = 'overvalued'
                else:
                    minPrice = round((z * quote.std2 + quote.mean2 + self.hedge * self.future) * 2)/2
                    if quote.perpBidPrice < minPrice:
                        self.trade_time = 0
                    else:
                        self.trade(quote)
                        self.closeType = 'undervalued'
        else:
            self.trade_time = 0

    def can_close(self, quote):
        if not self.in_position:
            return False
        if self.close_time == 0:
            if self.closeType == "undervalued" and quote.z2 <= 0 and quote.z1 <=0: 
                self.close_time = quote.Index + timedelta(seconds=2.5)
                self.get_responsible_asset(quote)
                self.perp = quote.perpAskPrice
                self.future = quote.futureBidPrice
                self.hedge = quote.hedge1
                self.trades.append({'time': quote.Index, 'perp': self.perp, 'perp_size': 0, 'future': self.future, 'future_size': 0, 'tradeType': 'exit signal', 'z': quote.z1})
                self.sustained_time = quote.Index + timedelta(seconds=2)
            if self.closeType == "overvalued" and quote.z1 >= 0 and quote.z2 >= 0:
                self.close_time = quote.Index + timedelta(seconds=2.5)
                self.get_responsible_asset(quote)
                self.perp = quote.perpBidPrice
                self.future = quote.futureAskPrice
                self.hedge = quote.hedge2
                self.trades.append({'time': quote.Index, 'perp': self.perp, 'perp_size': 0, 'future': self.future, 'future_size': 0, 'tradeType': 'exit signal', 'z': quote.z2})
                self.sustained_time = quote.Index + timedelta(seconds=2)
            return False
        sustained = self.sustained_z(quote)
        return sustained and quote.Index > self.close_time
    
    def close(self, quote):
        perp_size = -1 * self.net_position['perp']
        future_size = -1 * self.net_position['future']
        self.net_position['perp'] += perp_size
        self.net_position['future'] += future_size
        if self.closeType == "overvalued":
            perp = perp_size/quote.perpBidPrice
            future = future_size/quote.futureAskPrice
            z = quote.z2
        else:
            perp = perp_size/quote.perpAskPrice
            future = future_size/quote.futureBidPrice
            z = quote.z1
        self.position  = {'time': quote.Index, 'perp': perp, 'perp_size': perp_size, 'future': future, 'future_size': future_size, 'tradeType': 'close', 'z': z}
        self.trades.append(self.position)
        self.in_position = False
        self.positions.append(self.position)
        self.fees += 0.00075 * (abs(perp) + abs(future))
        self.max -= 0.00075 * (abs(perp) + abs(future))
        self.bets += 1
        self.close_time = 0
        self.market_volume = 0
        self.delever_prices = (0,0)
        self.liqudated = False
    
    def limit_close(self, quote):
        if not self.perpResponsible:
            if self.closeType == "undervalued":
                minPrice = round((self.perp + quote.std1 * 0.3 - quote.mean1) * 2/self.hedge)/2
                maxPrice = round((self.perp + quote.std1 * -0.3 - quote.mean1) * 2/self.hedge)/2
                if maxPrice > quote.futureBidPrice > minPrice:
                    self.close_time = 0
                else:
                    self.close(quote)
            else:
                minPrice = round((self.perp - quote.std2 * 0.3 - quote.mean2) * 2/self.hedge)/2
                maxPrice = round((self.perp + quote.std2 * 0.3 - quote.mean2) * 2/self.hedge)/2
                if maxPrice < quote.futureAskPrice < minPrice:
                    self.close_time = 0
                else:
                    self.close(quote)
        else:
            if self.closeType == "undervalued":
                minPrice = round((-0.3 * quote.std1 + quote.mean1 + self.hedge * self.future) * 2)/2
                maxPrice = round((0.3 * quote.std1 + quote.mean1 + self.hedge * self.future) * 2)/2
                if maxPrice < quote.perpAskPrice < minPrice:
                    self.close_time = 0
                else:
                    self.close(quote)
            else:
                minPrice = round((0.3 * quote.std2 + quote.mean2 + self.hedge * self.future) * 2)/2
                maxPrice = round((-0.3 * quote.std2 + quote.mean2 + self.hedge * self.future) * 2)/2
                if maxPrice > quote.perpBidPrice > minPrice:
                    self.close_time = 0
                else:
                    self.close(quote)

    
    def get_responsible_asset(self, quote):
        if self.tradeType == 'undervalued':
            if quote.perpAskPrice > self.last.perpAskPrice:
                perpRatio = quote.perpAskPrice/self.last.perpAskPrice
            else:
                perpRatio = self.last.perpAskPrice/quote.perpAskPrice
            if quote.futureBidPrice > self.last.futureBidPrice:
                futureRatio = quote.futureBidPrice/self.last.futureBidPrice
            else:
                futureRatio = self.last.futureBidPrice/quote.futureBidPrice
        else:
            if quote.perpBidPrice > self.last.perpBidPrice:
                perpRatio = quote.perpBidPrice/self.last.perpBidPrice
            else:
                perpRatio = self.last.perpBidPrice/quote.perpBidPrice
            if quote.futureAskPrice > self.last.futureAskPrice:
                futureRatio = quote.futureAskPrice/self.last.futureAskPrice
            else:
                futureRatio = self.last.futureAskPrice/quote.futureAskPrice
        self.perpResponsible = perpRatio > futureRatio

    def sustained_z(self, quote):
        if self.sustained_time == 0:
            return False
        if self.closeType == "undervalued":
            if quote.z1 > 0.2:
                self.sustained_time = 0
                self.close_time = 0
                return False
        else:
            if quote.z2 < -0.2:
                self.sustained_time = 0
                self.close_time = 0
                return False
        return quote.Index > self.close_time
    
    def get_liquidation_prices(self):
        if self.net_position['perp'] == 0 or self.net_position['future'] == 0:
            return (0,0)
        elif self.net_position['perp'] < 0:
            perp_liquidation = (self.net_position['perp_price'] * -1 * self.net_position['perp'])/(-1 * self.net_position['perp'] - self.net_position['perp_price'] * (self.max  + 0.005 * self.net_position['perp']/self.net_position['perp_price']))
            future_liquidation = (self.net_position['future_price'] * self.net_position['future'])/(self.net_position['future'] + self.net_position['future_price'] * (self.max - 0.005 * self.net_position['future']/self.net_position['future_price']))
        else:
            perp_liquidation = (self.net_position['perp_price'] * self.net_position['perp'])/(self.net_position['perp'] + self.net_position['perp_price'] * (self.max - 0.005 * self.net_position['perp']/self.net_position['perp_price']))
            future_liquidation = (self.net_position['future_price'] * -1 * self.net_position['future'])/(-1 * self.net_position['future'] - self.net_position['future_price'] * (self.max  + 0.005 * self.net_position['future']/self.net_position['future_price']))
        return (max(perp_liquidation,0), max(future_liquidation,0))

    def got_liquidated(self, quote):
        if not self.in_position:
            return False
        if self.liqudated:
            return False
        liquidation_prices = self.liquidation_prices
        if self.net_position['perp'] > 0:
            if all(i > 0 for i in liquidation_prices):
                if abs(quote.z1) < 5 and abs(quote.z2) < 5:
                    self.liquidated = quote.perpAskPrice <= liquidation_prices[0] or quote.futureBidPrice >= liquidation_prices[1]
        else:
            if all(i > 0 for i in liquidation_prices):
                if abs(quote.z1) < 5 and abs(quote.z2) < 5:
                    self.liqudated = quote.perpBidPrice >= liquidation_prices[0] or quote.futureAskPrice <= liquidation_prices[1]
        return self.liqudated

    def backtest(self, quotes, z):
        for quote in quotes.itertuples():
            if self.can_close(quote):
                self.close(quote)
                self.evaulate_profit()
            if self.can_trade(quote, z):
                self.limit_trade(quote, z)
            if self.got_liquidated(quote):
                print(self.get_liquidation_prices())
                print('Would have been liquidated here...')
            self.last = quote


    def evaulate_profit(self):
        revenue = 0
        for position in self.positions:
            revenue += position['perp'] + position['future']
        profit = round(revenue - self.fees, 8)
        self.profit.append(profit)
        print(f'You had a profit of {profit} XBT...')
        for position in self.positions[:-1]:
            self.max += 0.01 * (abs(position['perp']) + abs(position['future']))
        self.positions = []
        if profit > 0:
            self.winners += 1
        self.max += revenue
        self.max 
        self.total_capital += profit
        self.fees = 0

    def get_total_profit(self):
        print(f'Total profit: {sum(self.profit)} XBT......')

    def write(self,filename):
        with open(filename, "w") as file:
            headers = ['time', 'tradeType', 'perp', 'perp_size', 'future', 'future_size', 'z']
            csv_writer = DictWriter(file, fieldnames=headers)
            csv_writer.writeheader()
            for trade in self.trades:
                csv_writer.writerow(trade)
        
 
        
        
        


