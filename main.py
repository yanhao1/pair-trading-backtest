from data_cleaning import processData
from backtest2 import Backtester2

list = []
for i in range(20190401, 20190431):
    list.append(f"/warehouse/raw-data/{i}.csv")

for j in range(20190501, 20190532):
    list.append(f"/warehouse/raw-data/{j}.csv")

for k in range(20190601,200190631):
    list.append(f"/warehouse/raw-data/{k}.csv")


quotes = processData(list)

quotes['diff1']  = quotes['perpAskPrice'] - quotes['hedge1'] * quotes['futureBidPrice'] 
quotes['diff2']  = quotes['perpBidPrice'] - quotes['hedge2'] * quotes['futureAskPrice']

mean1 = quotes.diff1.mean()
mean2 = quotes.diff2.mean()
std1 = quotes.diff1.std()
std2 = quotes.diff2.std()

print(mean1, mean2, std1, std2)
print(quotes['hedge1'], quotes['hedge2'])
